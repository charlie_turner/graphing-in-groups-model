;;
;; Graphing in Groups v1.0.1 - for iOS clients
;; C. Turner 02-05-2015
;;
;; students own slope and y-intercept
;; pair of students control a line
;; two sets of lines on a graph
;; has correct syntax for equation

;; This model relies on two extensions:
;;   clickgraph does all the graphing of miniature graphs
;;   hubnet-web is the websocket-based version of hubnet, allowing communication with iOS clients
extensions [clickgraph hubnet-web]

globals [ 
  counter 
  list-of-functions1
  list-of-functions2
  graphernames  ;; list of all grapher names in the multi-display mode (i.e. ["PLOT1", "PLOT2", ..., "PLOTN"])
  collective?   ;; indicates whether we are in collective or multiple-graph display mode
  
  ;; log files
  mark-log
  move-log
  
  ;; display control variables
  coords?    ;; small integer that encodes how a point is labeled: 0="" 1=name 2=(x,y) 3= initial (x,y)
  show-lines?
  show-equations?
  show-labels?
  zoom-level
] 

breed [students student]
breed [dots dot]
breed [markers marker]

dots-own [
  group  ;;"1A", "3B", etc
  user-id
  x
  y
]

students-own [
  group  ;;"1A", "3B", etc
  user-id
  paired?
  x
  y
  slope 
  y-intercept
  equation-string
  equation
  colorname
  fraction-form
]

markers-own [
  group  ;;"1A", "3B", etc
  user-id
  paired?
  x
  y
  slope 
  y-intercept
  equation-string
  equation
  colorname
  fraction-form
]

to startup
  hubnet-reset
  hubnet-web:start 9999
  setup
end

to setup
  clear-all
  crt 1 [ set hidden? true ]   ;; create one hidden turtle - why is this here?  
  set coords? 1                ;; first three letters of name
  set counter 0
  set show-lines? true
  set show-equations? true
  set show-labels? false
  set collective? false
  set list-of-functions1 ["" "" "" "" "" "" "" ""]
  set list-of-functions2 ["" "" "" "" "" "" "" ""]

  setup-clickgraph
  setup-logfiles
  
  reset-ticks 
  show "Setup complete"
end


to setup-clickgraph
  clickgraph:setup min-pxcor max-pxcor min-pycor max-pycor patch-size
  clickgraph:workspace-create "PLOT" 2 4
  clickgraph:workspace-show "PLOT"  ;; have to explicitly show a workspace & its graphers
  set graphernames clickgraph:workspace-get-grapher-names "PLOT"
  clickgraph:workspace-create "COLLECTIVE" 1 1  ;; starts off invisible
  set zoom-level 1.0  ;; this is the default at which workspaces & graphers are created
  
  ;; turn on equations
  clickgraph:workspace-show-line-equations "PLOT" true
  ;; don't ever want to show equations in collective graph
  clickgraph:workspace-show-line-equations "COLLECTIVE" false
  
  switch-to-individual-graphs
end


;composes a string from date and time based upon flags
to-report make-filename [log-type]
  let months ["Months" "Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec"]
  let dttm date-and-time
  let month position (substring dttm 19 22) months
  ifelse month < 10 [set month (word "0" month)][set month (word month)]
  let tim substring dttm 0 5
  while [member? ":" tim][set tim replace-item (position ":" tim) tim "."]
  let result (word substring dttm 23 27 "." month "." substring dttm 16 18 " " tim substring dttm 13 15  " GG8-" log-type ".txt")
  report result
end


to go
  carefully [
    ;; get commands and data from the clients
    listen-clients
  ]
  [print error-message]
  tick
end

to listen-clients
  while [hubnet-message-waiting?] [
      hubnet-fetch-message
      ifelse hubnet-enter-message? [ 
          create-new-student
      ][
          ifelse hubnet-exit-message? [
            remove-student
          ][
            execute-command hubnet-message
          ]
      ]
   ]
end


;; execute-command: detailed command processor for messages received from clients
;; 
to execute-command [command]
  let current-student (one-of students with [user-id = hubnet-message-source])
  let current-marker (one-of markers with [user-id = hubnet-message-source])
  let current-group [group] of current-student
  let cmd-prefix crop-command command
  
  show (word "execute-command: " hubnet-message-source " " hubnet-message-tag " " cmd-prefix) 

  ;; switch on the message-tag, using 'stop' command to go to end of procedure 
  if hubnet-message-tag = "set-group" [set-group stop]
  if hubnet-message-tag = "move-point" [move-point command stop]
  if hubnet-message-tag = "mark-point" [mark-point stop]
  if hubnet-message-tag = "draw-image" [draw-image stop]

  show (word "execute-command - unrecognized command: " hubnet-message-tag " from " hubnet-message-source)
end

;; crop-command: truncate really long commands (e.g. draw-image with the hex encoded image as argument)
to-report crop-command [command]
  ifelse length command > 50 [
    report (word substring command 0 50 " ...")
  ] [
    report command
  ]
end


;;---------------------------------------------------------------------------------------
;;
;;   SUB COMMANDS - dispatched on hubnet-message-tag
;;
;;---------------------------------------------------------------------------------------

to set-group
  let g hubnet-message
  ifelse count students with [group = g] >= 2 [
    ;; this is a hack to tell the client that this group is full.  Overloading meaning of graph-dim!
    hubnet-send hubnet-message-source "set-graph-dim" 0
  ][
    setup-group hubnet-message
    hubnet-send hubnet-message-source "set-graph-dim" dim
    ;; adding new code to share group information 11-3-13 cjt
    add-group-members g
  ] 
end

to move-point [command]
  set command (read-from-string command) ;;# convert command string to list
  ask students with [user-id = hubnet-message-source][
    set x (first command)
    set y (last command)
    adjust-position zoom-level
    share-with-group-member group x y 
    record-move
    if coords? = 2 [set label (word "(" x ", " y ")   ")]
    if coords? = 3 [set label (word first user-id "(" x ", " y ")   ")]
  ]
end

;; mark-point: triggered by student that they want their point "marked"
;;  marking a point triggers an update to the group's line equation, which in turn updates the 
;;  line that is displayed, and the equation above/below the graph

to mark-point
  ask students with [user-id = hubnet-message-source] [
    
    ask markers with [user-id = hubnet-message-source] [die]
    hatch-markers 1 [
      set user-id hubnet-message-source
      set group [group] of one-of students with [user-id = hubnet-message-source]
      set shape "x"
      set label ""
      update-line  ;; recalculate new line parameters - updates equation vars in markers

      ;; graph the updated line
      graph-updated-mark group 
      record-mark  ;; log event
    ]
    
    ;; right now the updated equation is only in the markers.  Duplicate that info into the appropriate students
    set equation [equation] of one-of markers with [user-id = hubnet-message-source]
 
    ;; update both clients of group whenever one performs a mark
    mark-with-group-member group x y
  ]
end

;; draw-image: display the image sent from client
;;   output image to both the appropriate multi-graph AND the collective graph
to draw-image
  ask students with [user-id = hubnet-message-source ][
    let gname (word "PLOT" first group) ;; graph name
    clickgraph:grapher-draw-image gname hubnet-message
  ]
  clickgraph:grapher-draw-image "COLLECTIVE" hubnet-message
end


;;---------------------------------------------------------------------------------------
;;
;;   END of SUB COMMANDS
;;
;;---------------------------------------------------------------------------------------


;; grapher-name: reporter that knows how workspaces name their graphers
;;   workspace name: PLOT => graphers named PLOT-01, PLOT-02, ... PLOT-NN
to-report grapher-name [workspace-name plot-number]
  let pnum-str (word plot-number)
  let pnum-len length pnum-str
  report (word workspace-name "-" substring "00" 0 (2 - pnum-len) pnum-str)
end

;; code to send my coords to other member of my group whenever I move
to share-with-group-member [grp sx sy]
  ask students with [group = grp][
    if user-id != hubnet-message-source [
      hubnet-send user-id "show-partner" (list hubnet-message-source sx sy)]  ;; (word "{\"x2\": " sx ", \"y2\": " sy "}")    ]
  ]
end

;;# new code to send my partner's coords back to me - when I first log in  - cjt
to get-group-member-pos [grp]
  ask students with [group = grp][
    if user-id != hubnet-message-source [
      hubnet-send hubnet-message-source "add-partner" (list user-id grp x y colorname)] 
  ]
end

;; exchange partner info when I first log in
to add-group-members [grp]
  if count students with [group = grp] = 2 [

    ask students with [group = grp] [
      ;; student who triggered this exchange
      ifelse user-id = hubnet-message-source [
        let st2 one-of students with [group = grp and user-id != hubnet-message-source] 
        hubnet-send [user-id] of st2 "add-partner" (list user-id grp x y colorname)
      ][
        let st1 one-of students with [group = grp and user-id = hubnet-message-source] 
        hubnet-send hubnet-message-source "add-partner" (list user-id grp x y colorname)
      ]
    ]
  ]
end


;; sends me my group members last marked point
;; it also has to send them my just-marked position
;; is also updates the equations in each client - same as show!
to mark-with-group-member [grp sx sy]
  
   ;; this code is only relevant if there are two members in the group - otherwise just ignore the mark command
   if count markers with [group = grp] = 2 [
     
      ;; create a list of markers
      let marker-list [ self ] of markers with [group = grp] 
      let marker1 item 0 marker-list
      let marker2 item 1 marker-list

      hubnet-send [user-id] of marker1 "mark-partner" (list [user-id] of marker2 [x] of marker2 [y] of marker2)
      hubnet-send [user-id] of marker2 "mark-partner" (list [user-id] of marker1 [x] of marker1 [y] of marker1)
      
      ;; if the teacher has enabled equation display then send equations to clients
      ifelse show-equations? [
        ;; showing equation as fractions?
        ifelse fraction? [
          hubnet-send [user-id] of marker1 "show-equation" [fraction-form] of marker1
          hubnet-send [user-id] of marker2 "show-equation" [fraction-form] of marker1
        ][
          ;; showing coefficients as decimals
          hubnet-send [user-id] of marker1 "show-equation" [equation] of marker1
          hubnet-send [user-id] of marker2 "show-equation" [equation] of marker1
        ]
      ][
         ;; otherwise do nothing for now.
         hubnet-send [user-id] of marker1 "show-equation" "NONE"
         hubnet-send [user-id] of marker2 "show-equation" "NONE"
      ]
    ]

end

to setup-group [g]
  ask students with [user-id = hubnet-message-source] [
    ifelse group = "N/A" [   
      ifelse count students with [group = g] = 0 [
        initiate-first g
      ]
      [
        ifelse count students with [group = g] = 1 [
          initiate-second g
      ]
        []
      ]
    ]
    [stop]
  ]
end


;;initiate first group in graph
to initiate-first [g]
  
  set group g
  ifelse last g = "Y" [
    set color yellow
    set colorname "YELLOW"
  ][
    set color lime  
    set colorname "GREEN"
  ]
  set x 0
  set y 0
  
  ifelse collective? [
    let cor clickgraph:grapher-to-patch-coord "COLLECTIVE" x y
    setxy item 0 cor item 1 cor
  ][
    let grph (word "PLOT" first group)
    let cor clickgraph:grapher-to-patch-coord grph x y
    setxy item 0 cor item 1 cor
  ]
  set hidden? false
end

;;initiate second group in graph
to initiate-second [g]
  
  set group g
  ifelse last g = "Y" 
  [set color yellow
    set colorname "YELLOW"]
  [set color lime      
    set colorname "GREEN"]
  
  set x 0
  set y 0
  
  ifelse collective? [
    let cor clickgraph:grapher-to-patch-coord "COLLECTIVE" x y
    setxy item 0 cor item 1 cor
  ][
    let grph (word "PLOT" first group)
    let cor clickgraph:grapher-to-patch-coord grph x y
    setxy item 0 cor item 1 cor
  ]

  set hidden? false
  ask students with [group = g][
    set paired? true
  ]
end


;; graph-updated-mark: - called from mark-point.  After a student
;;   has just clicked "mark", and wants their line updated.  This function does the 
;;   graphing of the updated line

to graph-updated-mark [mygroup]
  let plotname (word "PLOT" (first mygroup))
  let marker-set markers with [group = mygroup]
  if count marker-set = 2 [
    ask marker-set [
      ;; update non-vertical lines
      ifelse equation != "N/A" [
        clickgraph:equation-add-custom plotname group equation-string colorname "thick" 
        clickgraph:equation-add-custom "COLLECTIVE" group equation-string colorname "thick"
      ][
        ;; erase vertical lines
        clickgraph:equation-delete plotname group
        clickgraph:equation-delete "COLLECTIVE" group
      ]
    ]
    clickgraph:regraph
  ]
  
end

;; set-group-equation: update the equations above (and below) the 8 graphers 
;;   this is the text string form (not the line)

to set-group-equation [grp equ]
  let plotnum item 0 grp ;; graph number
  let groupcolor item 1 grp ;; Yellow or Green
  clickgraph:grapher-set-group-equation (word "PLOT" plotnum) groupcolor equ
end

;; update-line: called when any student performs a "mark" operation.  At this time,
;; provided the student is part of a 2 person group (a line), all line variables are recalculated
;; This code is in it's original form from graduate student days

to update-line
  let g [group] of one-of markers with [user-id = hubnet-message-source]
  let s "N/A"
  let yi "N/A"
  let yfrac "N/A"
  let eq "N/A"
  let eq-str "N/A"
  let eq-frac "N/A"
  
  if count markers with [group = g] = 2 [
    let marker-set markers with [group = g]
    let marker1 one-of marker-set
    let marker2 one-of marker-set with [user-id != [user-id] of marker1]
    
    ;; check to see if x coordinates are equal -> a vertical line
    ifelse [x] of marker2 = [x] of marker1 [
      show (word "Verical line between " [user-id] of marker1 " and " [user-id] of marker2)
      ask marker-set [
        set slope s
        set y-intercept yi
        set equation eq-str
        set fraction-form eq-str
      ]
    ][
      let denominator ([x] of marker2 - [x] of marker1)
      let numerator ([y] of marker2 - [y] of marker1)
      
      set s (([y] of marker2 - [y] of marker1)/([x] of marker2 - [x] of marker1))
      set yi precision (([y] of marker2)  - (s * [x] of marker2)) 2
      set yfrac reduce-frac ([y] of marker1 * denominator - numerator * [x] of marker1) denominator
      
      let sign ""  
      ifelse (denominator * numerator > 0) 
      [set sign "pos"] [set sign "neg"]
      
      set denominator (abs denominator)
      set numerator (abs numerator)     
      
      let big 0
      let small 0
      let gcd 0
      
      ifelse numerator > denominator  [
        set big numerator set small denominator
      ][
        set small numerator set big denominator
      ]
      
      ifelse numerator = 0 [
        set s 0
        ][
          ifelse sign = "pos" [
            set s reduce-frac numerator denominator
          ][
            set s word "-" reduce-frac numerator denominator
          ]
        ]
      
      ifelse s = 0 [
        ifelse yi = 0 [
          set eq "0"
          set eq-frac "0"
          set eq-str "0"
        ][
          set eq (word yi "")
          set eq-str (word yi "")
          set eq-frac (word yi "")
        ]
      ][
        ;; in the lines that follow, very important to use '*' so that clickgraph equation solver works correctly 
        ifelse yi = 0 [
          ifelse s = 1 [
            set eq (word "X")
            set eq-str (word "X") 
            set eq-frac (word "X") 
          ][
            set eq (word s " * X") 
            set eq-str (word "(" s ")" " * X") 
            set eq-frac (word s " * X") 
          ]
        ][
          ifelse s = 1 [
            set eq (word "X" "+" (word yi ""))
            set eq-str (word "X" "+" (word yi ""))
            set eq-frac (word "X" "+" yfrac) 
          ][
            set eq (word s " * X" "+" (word yi "")) 
            set eq-str (word "(" s ")" " * X" "+" (word yi ""))    
            set eq-frac (word s " * X" "+" yfrac)  
          ]
        ]
      
      ]
      
      if position "+-" eq != false 
      [set eq remove "+" eq] 
      if position "+-" eq-frac != false 
      [set eq-frac remove "+" eq-frac] 
      
      ;; update both markers 
      ask marker-set [
        set slope s
        set y-intercept yi
        set equation eq
        set equation-string eq-str
        set fraction-form eq-frac
      ]
    ]

    ifelse fraction? 
      [set-group-equation g (word "Y = " eq-frac)]
      [set-group-equation g (word "Y = " eq)]    
  ]
end


to-report reduce-frac [n d]
  let sign ""  
  ifelse (d * n > 0) 
    [set sign "pos"] [set sign "neg"]
  
  set d (abs d)
  set n(abs n)     
  
  let big 0
  let small 0
  let gcd 0
  
  ifelse n > d [set big n set small d]
    [set small n set big d]
  
  ifelse n = 0
    [report 0]
    [ifelse (remainder n d) = 0
      [
        
        ifelse sign = "pos" 
          [report (precision (n / d) 0)]
          [report -1 * (precision (n / d) 0)]
      ]
    
    ;;reducing fraction 
    
      [while [small != 0]
        [set gcd small
          set small (remainder big small)
          set big gcd
          
        ]
      set n precision (n / gcd) 0
      set d precision (d / gcd) 0
      ifelse sign = "pos" 
        [
          report (word n "/" d) 
        ]
        [
          report (word (-1 * n) "/" d)
        ]     
      ]
    ]
  
end

;;TO CREATE OR REMOVE STUDENTS
to create-new-student
  show (word "create new student: " hubnet-message-source)
  create-students 1
  [set user-id hubnet-message-source
    set color (item (random 8) (list yellow cyan pink lime orange magenta white red sky )) 
    set shape "circle 3"
    
    ;; set the new student's label - at this point we don't know (x, y) so choose between show/noshow name
    ifelse coords? = 0 [
      set label ""
    ][
      set label (word substring hubnet-message-source 0 3 "   ")
    ]
    
    set paired? false
    set hidden? true
    set group "N/A"
    set equation "N/A"
    set equation-string "N/A"
    file-open move-log
    file-print (word timer "\t" user-id "\t\tlogged on")
    file-close 
  ]
end


to remove-student
  let g [group] of one-of students with [user-id = hubnet-message-source]
  show (word "student " hubnet-message-source " (in group " g ") is exiting")
  
  ifelse g != "N/A" [
    let plotname (word "PLOT" (first g))
    let student1 one-of students with [user-id = hubnet-message-source] 
    
    if [paired?] of student1 != false [
      let student2 one-of students with [group = g and user-id != [user-id] of student1]
      ask student2 [
        clickgraph:equation-delete plotname g  ;; [equation-string] of student2
        clickgraph:equation-delete "COLLECTIVE" g ;; this is the equation of the line on the collective graph

        set paired? false
        set equation "N/A"
      ]
      
      ;; let partner's of the exiting student know - this must be done before we kill this student
      remove-group-member g
    ]
    
    ask students with [user-id = hubnet-message-source][die]
    ask markers with [user-id = hubnet-message-source][die]
    ask dots with [user-id = hubnet-message-source][die]  
    
    set-group-equation g ""  ;; this is the displayed equation above the graph
    
    ask students with [group = g and equation != "N/A" and paired? = true] [
      set equation "N/A" 
      set paired? false
    ]
     
    clickgraph:regraph
  ]
  [
    show "Student tried to join a full group... exiting"
    ask students with [group = "N/A"] [die]
  ]
  
end



;; send partner of the student that exited a remove-partner message
to remove-group-member [grp]
  if count students with [group = grp] = 2 [
    ask students with [group = grp and user-id != hubnet-message-source] [
      hubnet-send user-id "remove-partner" (list hubnet-message-source grp)
    ]
  ]
end

to dump-equations 
  let gname (word "PLOT" (word num-plot ""))
  clickgraph:grapher-dump-equations gname 
  clickgraph:grapher-dump-equations "COLLECTIVE"
end

;;graph the first teacher's function (triggered by button)
to graph-function1 
  ;; plot to be updated controlled by num-plot slider in UI
  let pname (word "PLOT" (word num-plot ""))
  
  ;; update just this grapher's function in list-of-functions1
  set list-of-functions1 (replace-item (num-plot - 1) list-of-functions1 function1)
  clickgraph:equation-add-custom pname "teacher1" (item (num-plot - 1) list-of-functions1) "white" function1-type
  clickgraph:regraph 
end

;;graph the second teacher's function (triggered by button)
to graph-function2 
  ;; plot to be updated controlled by num-plot slider in UI
  let pname (word "PLOT" (word num-plot ""))
  
  ;; update just this grapher's function in list-of-functions1
  set list-of-functions2 (replace-item (num-plot - 1) list-of-functions2 function2)
  
  ;; equation-add-custom will create a new equation or update the parameters to current values
  clickgraph:equation-add-custom pname "teacher2" (item (num-plot - 1) list-of-functions2) "pink" function2-type 
  clickgraph:regraph 
end

;;graph the second teacher's function (triggered by button)
to graph-collective
  ;; equation-add-custom will create a new equation or update the parameters to current values
  clickgraph:equation-add-custom "COLLECTIVE" "teacher3" function3 "red" function3-type  
  clickgraph:regraph 
end

;;graphing the teacher's function
to graph-all
  file-open mark-log
  file-print (word timer "\tTEACHER\t\t\t" function1)
  file-close
  
  ;; loop over all the graphers and add/update the functions
  foreach graphernames [
    ;; equation-add-custom will create a new equation or update the parameters to current values
    clickgraph:equation-add-custom ? "teacher1" function1 "white" function1-type
    clickgraph:equation-add-custom ? "teacher2" function2 "pink" function2-type
  ]

  ;; update all graphers' functions to the same thing
  set list-of-functions1 map [function1] list-of-functions1
  set list-of-functions2 map [function2] list-of-functions2 

  clickgraph:regraph
end

;; erase all teacher functions set through UI.
;; two are in the multiple plots (PLOT1, PLOT2, ... PLOT8): teacher1, teacher2
;; one is in the COLLECTIVE graph: teacher3
to clear-all-functions
  
  foreach graphernames [
    clickgraph:equation-delete ? "teacher1"
    clickgraph:equation-delete ? "teacher2"
  ]
  clickgraph:equation-delete "COLLECTIVE" "teacher3"
  
  set list-of-functions1 ["" "" "" "" "" "" "" ""]
  set list-of-functions2 ["" "" "" "" "" "" "" ""]

  clickgraph:regraph
end

;; scaled down show lines!  See below
to show-lines
  set show-lines? true
  clickgraph:workspace-show-lines "PLOT" true
  clickgraph:workspace-show-lines "COLLECTIVE" true
  clickgraph:regraph
end

;;
;; TODO: show-lines: figure out what all this is with dots and white lines
;;
;;EQ to show-lines-with-dots-not-tested
;;EQ   set show-lines? true
;;EQ   
;;EQ   ask markers with [equation != "N/A"] [ 
;;EQ     clickgraph:equation-add-custom (word "PLOT" first group) (word "graph" group) equation "white" "thick"
;;EQ     clickgraph:equation-hide (word "PLOT" first group) (word "graph" group)
;;EQ 
;;EQ     let g group 
;;EQ     ifelse count dots with [first group = first g] > 0 [
;;EQ       let pt one-of dots with [first group = first g]
;;EQ       ifelse [y] of pt = (last clickgraph:equation-get-value (word "PLOT" first group) (word "graph" group) 1 [x] of pt)[
;;EQ         clickgraph:equation-delete (word "PLOT" first group) (word "graph" group)
;;EQ         clickgraph:equation-add-custom (word "PLOT" first group) "graph" equation-string "YELLOW" "thick"
;;EQ         clickgraph:equation-show (word "PLOT" first group) "graph"
;;EQ       ] [
;;EQ         clickgraph:equation-delete (word "PLOT" first group) (word "graph" group)
;;EQ         clickgraph:equation-add-custom (word "PLOT" first group) "graph" equation "GREEN" "thick"
;;EQ       ]
;;EQ         
;;EQ       clickgraph:equation-show (word "PLOT" first group) "graph"
;;EQ     ][
;;EQ       ask markers with [equation != "N/A"] [clickgraph:equation-add-custom (word "PLOT" first group) "graph" equation-string colorname "thick"]
;;EQ     ]
;;EQ   ]
;;EQ   
;;EQ   clickgraph:regraph
;;EQ end


to hide-lines
  set show-lines? false

  clickgraph:workspace-show-lines "PLOT" false
  clickgraph:workspace-show-lines "COLLECTIVE" false
  clickgraph:regraph
end

;; show/hide-axes: called from UI to toggle display of plot axes
to show-axes
  foreach graphernames [clickgraph:grapher-show-axes ? true]  ;; loop over multiple graphs
  clickgraph:grapher-show-axes "COLLECTIVE" true ;; handle collective graph
end

to hide-axes
  foreach graphernames [clickgraph:grapher-show-axes ? false]  ;; loop over multiple graphs
  clickgraph:grapher-show-axes "COLLECTIVE" false ;; handle collective graph
end

;; show/hide-equations: called from UI to toggle textual display of equations
;;   above and below multi-graphs.  Does not apply to collective graph
to show-equations
  set show-equations? true
  ;; turn on equations for all graphers in the PLOT workspace (multi-graphs)
  clickgraph:workspace-show-line-equations "PLOT" true
end

to hide-equations
  set show-equations? false
  clickgraph:workspace-show-line-equations "PLOT" false
  ;; get all the clients to erase their equations
  ;; this is a hack needed because we don't send globals to clients when they change
  ask markers [hubnet-send user-id "show-equation" "NONE"]
end


;;; 
;;; Point plotting functions - for drawing teacher-specified colored dots on each graph at the same location
;;;   as defined by user input "point" 

to plot-all
  ifelse collective? 
    [ plot-point "COLLECTIVE" ]
    [ foreach graphernames [plot-point ?]]
end

to plot-point [grph]
    create-dots 1 [
      set color (item (random 8) (list yellow cyan pink lime orange magenta white red sky)) 
      set user-id "teacher"
      set shape "circle"
      set group last grph
      set x first (read-from-string point) 
      set y last (read-from-string point)
      let cor clickgraph:grapher-to-patch-coord grph x y
      setxy item 0 cor item 1 cor
    ]
end

to clear-all-points
  ask dots with [user-id = "teacher"] [die]
end

to show-names
  set coords? 1
  ask students [set label (word subString user-id 0 3 "   ")]
end

to show-coords
  set coords? 2
  ask students [set label (word "(" x ", " y ")   ")]
end

to show-init-coords
  set coords? 3
  ask students [set label (word first user-id "(" x ", " y ")   ")]
end

to no-show
  set coords? 0
  ask students [set label ""]
end


to switch-to-collective-graph
  carefully [
    set collective? true
    clear-all-points
    clickgraph:workspace-hide "PLOT"
    clickgraph:workspace-show "COLLECTIVE"

    ask students [adjust-position zoom-level]
    
    ;; draw markers no matter what
    ask markers [adjust-position zoom-level]
    clickgraph:regraph
  ]
  [print error-message]
end

to switch-to-individual-graphs
  carefully [
    set collective? false 
    clear-all-points  ; clear any points drawn with NetLogo
    clickgraph:workspace-hide "COLLECTIVE"
    clickgraph:workspace-show "PLOT"
 
    ask students [adjust-position zoom-level]

    ; draw markers no matter what
    ask markers [adjust-position zoom-level]
    clickgraph:regraph
  ]
  [print error-message]
end

;; record functions: for saving moves and marks to log files in model directory
;;

to setup-logfiles
  set mark-log make-filename "mark"
  if not file-exists? mark-log [
    file-open mark-log 
    file-print (word "CTIME\tGROUP\tACTOR\tXVAL\tYVAL\tEQUATION")
    file-close
  ]
  set move-log make-filename "move"
  if not file-exists? move-log [
    file-open move-log
    file-print (word "CTIME\tGROUP\tACTOR\tXVAL\tYVAL") 
    file-close
  ]
end


to record-move
  file-open move-log
  file-print (word timer "\t" group "\t" user-id "\t" x "\t" y)  
  file-close
end

to record-mark
  file-open mark-log
  file-print (word timer "\t" group "\t" user-id "\t" x "\t" y "\t" equation)
  file-close 
end

to record
  file-open mark-log
  file-print (word timer "\tTEACHER\t\t\t" instruction)
  file-close
  file-open move-log
  file-print (word timer "\tTEACHER\t\t\t" instruction)
  file-close
end

;; clear-images: remove all displayed images from multi- and collective graphs
;; 
to clear-images
  foreach graphernames [clickgraph:grapher-clear-images ?]
  clickgraph:grapher-clear-images "COLLECTIVE"
  clickgraph:regraph
end

to adjust-position [new-zoom-level] ;; dot, marker, student procedure
  let grph get-grapher-name
  let cor clickgraph:grapher-to-patch-coord grph x y
  setxy item 0 cor item 1 cor
  let border dim / new-zoom-level
  ifelse x >= border or y >= border or x <= (- border) or y <= (- border) 
    [hide-turtle]
    [show-turtle]
end

to-report get-grapher-name ;; dot, marker, student reporter
  ifelse collective? 
    [report "COLLECTIVE"]
    [report (word "PLOT" first group)]
end
@#$#@#$#@
GRAPHICS-WINDOW
233
10
1099
529
53
30
8.0
1
16
1
1
1
0
1
1
1
-53
53
-30
30
1
1
1
ticks
30.0

BUTTON
115
43
219
76
Read Msgs
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
1106
101
1278
134
num-plot
num-plot
1
8
1
1
1
NIL
HORIZONTAL

BUTTON
1112
262
1223
295
NIL
graph-function1
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1230
303
1310
336
NIL
graph-all
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1111
304
1224
337
NIL
clear-all-functions
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
30
202
119
235
NIL
show-lines
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
202
217
235
NIL
hide-lines
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
32
149
109
194
NIL
show-lines?
3
1
11

BUTTON
18
240
118
273
NIL
show-equations
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
240
217
273
NIL
hide-equations
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
116
149
228
194
NIL
show-equations?
3
1
11

BUTTON
117
492
207
525
NIL
clear-all-points
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
116
449
211
482
NIL
plot-all
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1268
22
1337
55
NIL
record
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
1107
23
1260
83
instruction
DONE
1
0
String

INPUTBOX
1106
146
1222
206
function1
(2/3) * X - 4.0
1
0
String

INPUTBOX
19
450
109
510
point
[-2 3]
1
0
String

BUTTON
17
281
118
314
NIL
show-names
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
280
225
313
NIL
show-coords
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
122
319
224
352
NIL
no-show
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
1229
146
1338
206
function2
X
1
0
String

CHOOSER
1121
211
1222
256
function1-type
function1-type
"thick" "thin" "above" "below"
0

CHOOSER
1229
211
1329
256
function2-type
function2-type
"thick" "thin" "above" "below"
2

BUTTON
1229
262
1331
295
NIL
graph-function2
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
17
88
109
121
One Graph
switch-to-collective-graph
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
115
88
219
121
Many Graphs
switch-to-individual-graphs
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
6
396
110
429
fraction?
fraction?
0
1
-1000

SLIDER
17
43
109
76
dim
dim
10
10
10
10
1
NIL
HORIZONTAL

INPUTBOX
1112
359
1223
421
function3
-1/2*X + 5
1
0
String

BUTTON
1105
481
1224
514
NIL
graph-collective\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
9
10
189
38
Press 'Read Msgs' Button to start\n
11
0.0
1

BUTTON
7
319
118
352
NIL
show-init-coords
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
1113
429
1224
474
function3-type
function3-type
"thick" "thin" "above" "below"
3

BUTTON
18
357
118
390
NIL
show-axes
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
123
357
217
390
NIL
hide-axes
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
1230
358
1342
391
NIL
clear-images
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
21
114
215
139
___________________
20
106.0
1

TEXTBOX
18
416
214
442
___________________
20
106.0
1

@#$#@#$#@


## INTRODUCTION 

Graphing in Groups is a collaborative learning environment developed for mathematics classrooms. Students working in pairs construct graphs of linear functions by using their respective iOS devices to move points in a shared Cartesian window. The Graphing in Groups hubnet model allows a teacher to view and display the graphing windows for all student pairs. Two pairs, one yellow and one green, can form lines on in each graphing window. As students move their points in their respective graphing windows, they can mark select locations to form lines, the y=mx+b equation for which is displayed above or below the graph. This configuration allows students to participate in a variety of structured or open-ended collaborative mathematics tasks involving generating lines with particular properties - specific slope or intercept, a parallel or perpendicular relationship to another group's line, passage through a select point, and so on.


Teachers can specify whether graphs, equations, coordinate points are shown or hidden. All lines can be collected into a single display and teachers can stamp target points or example lines onto the collective or individual displays.


## NETLOGO MANUAL 

## STARTING THE MODEL 

TEACHERS: Click the "Read Msgs" button to activate the network.  The network will remain activated until the "Read Msgs" button is clicked again to deactivate it. Teachers can use this button to "freeze" the activity and gain student attention.  Clicking the "HubNet Control Center" item in the "Tools" menu opens a window that displays the activity of all the students on the network.



    Widget                     Type            Description
    ------                     ----            -----------
    "Read Msgs"                toggle          Activates and deactivates the network.
    "HubNet Control Center"    menu item       Opens network control center window.

Take note of the "Server address" as shown in the "HubNet Control Center" window (e.g. 10.0.1.2). Students will need to enter this value exactly as it appears in order to join the NetLogo network.  They will also need to know the exact version of NetLogo you are running (e.g. 5.0.4).

STUDENTS: Start the "Graphing in Groups" iOS application on your iPod, iPhone or iPad. On the Welcome page, enter your name or initials (up to seven characters).  From the graph window, select the "NetLogo" settings button in the upper left corner of the screen. On the settings screen, enter four additional pieces of information: 1) the teacher's "Server address", 2) the version of NetLogo the teacher is using, 3) a graph color (yellow or green) and 4) a graph number (1-8). Each student pair must choose the same graph number and the same color to be able to construct the same line.  After selecting "Save" in the upper right, the graph window will reappear with a new "Join" button on the right.  Select "Join", to connect to the teacher's computer. Your name should appear in both the "HubNet Control Center" and as a labeled point in the graph window you selected.

Detailed instructions and screen shots can be found on this webpage:
http://click.ucdavis.edu/designs/graphing-in-groups/collaborative-graphing-in-groups/


## SWITCHING DISPLAY MODES (ONE OR MANY GRAPHS) 

TEACHERS: Graphing in Groups provides many powerful ways to configure the collective display.  There are two main modes that can be switched by clicking the "One Graph" and "Many Graphs" buttons.  The model starts out in many graphs mode with two pairs of students assigned to each graph display window.  The window the displays the graph of a pair corresponds to the group number they chose when they logged in.  Clicking the "One Graph" button collects all the lines and displays them in a single large graph.  To return to the eight graph display click "Many Graphs".  The dimensions of the graph is currently fixed to show either 10 units in each direction from the origin as indicated by the "dim" slider.



    Widget                    Type        Description
    ------                    ----        -----------
    "One Graph"               button      One graphical display window.
    "Many Graphs"             button      8 graphical display windows.
    "dim"                     slider      10 unit displays



## CONFIGURING LINE, COORDINATE AND EQUATION DISPLAY 

TEACHERS:  Lines, coordinates and equations can be displayed or hidden and the display can be configured in several ways.  Lines can by shown by clicking the "show-lines" button and hidden with the "show-no-lines" button.  Coordinates can be shown with the "show-coords" button and hidden with the "no-show" button.  The username of the student can be displayed next to the coordinate point by clicking the "show-names" button. The "show-init-coords" button displays the first letter of the student's name and the coordinates of the point.  Clicking the "show-equations" button displays the equation of each line above or below the graphical display.  The equations can be hidden with the "show-no-equations" button.  Setting the "fractions?" switch to on displays coefficients as fractions in the equation.  Setting "fractions?" to off displays decimal coefficients.



    Widget                    Type        Description
    ------                    ----        -----------
    "show-lines"              button      Shows all pair's lines
    "show-no-lines"           button      Hides all lines
    "show-coords              button      Shows coordinates of points
    "no-show"                 button      Hides names and coordinates
    "show-names"              button      Shows names of points
    "show-init-coords"        button      Shows first letter of name and coordinates
    "fraction?"               switch      Activates fraction display in equations


## STAMPING POINTS AND LINES ON THE GRAPH DISPLAY

TEACHERS:  Lines, points, and inequality graphs can be stamped onto the graphical display.  

To stamp a point onto all 8 graph windows, set the display to "Many Graphs," enter the coordinates "point" window (e.g.[2 7]) and press the enter key.  Make sure that the coordinates inside the square brackets are separated by a space (not a comma).  Click the "plot-all" button to plot the point in all the windows.  Repeating this process adds additional points to the graphical displays.  Click the "clear-all-points" button to clear all points from all the windows.  


    Widget                    Type        Description
    ------                    ----        -----------
    "point"                   window      Enter coordinates (e.g. [2 7]) to plot.
    "plot-all"                button      Plots the "point" in all windows.
    "clear-all-points"        button      Clears all points from all windows.


To display a line, enter the linear function into the "function1" or "function2" window and press the Enter key.  Click the "graph-function1" button to display a white-colored linear graph of function1 in the graph window indicated by the "num-plot" slider. Clicking the "graph-function2" button displays function2 in red.  Moving the "num-plot" slider changes the target window for line.  To plot both function1 and function2 on all the windows click the "graph-all" button.  Click the "clear-all-functions" button to clear all the lines.  Erasing a function from either the "function1" or "function2" window and clicking the "graph-all" button plots only the remaining function.  Inequalities can be plotted by adjusting the "function1-type" or the "function2-type" selector to "above" or "below" and pressing the corresponding graph button.  Selecting "thin" resets the display to a line. plotting function1 and function2 works only in "Many Graphs" mode.


    Widget                    Type        Description
    ------                    ----        -----------
    "function1"               window      Function to plot in white.
    "graph-function1"         button      Plots function1 in the "num-plot" graph.
    "function2"               window      Function to plot in red.
    "graph-function2"         button      Plots function2 in the "num-plot" graph.
    "num-plot"                slider      Target graph function plots.
    "graph-all"               button      Plots all functions to all graphs.
    "clear-all-functions"     button      Clears all lines on all graphs.
    "function1-type"          selector    Sets function1 to lines or inequalities.
    "function2-type"          selector    Sets function2 to lines or inequalities.
    
    --NOTE: Works only in "Many Graphs" mode.


A function can be stamped on to the "One Graph" mode display.  Enter the function to be displayed in the "function3" window.  Click the "graph-collective" button to plot the function onto the collective display.  Erasing the function in the "function3" window and clicking the "graph-collective" button will erase the line from the graph.  Inequalities cannot be graphed in this the "One Graph" mode.



    Widget                    Type        Description
    ------                    ----        -----------
    "function3"               window      Function to plot on collective display.
    "graph-collective"        button      Plots function3 on the collective display.
    
    --NOTE: Works only in "One Graph" mode.


## LOG FILES

TEACHERS: Each time you run NetLogo, two new log files will be created in the models directory - one for student moves, the other for times when points are "marked". The file names will include today's date, the time, and either GG8-move.txt or GG8-mark.txt. You can use the "Instruction" text window and "Record" button in the upper right of the display to insert words into the log files at any time.  For example, you could type the phrase "START EX ONE" into the "Instruction" box, then press "Record" before you begin the first exercise; ending the exercise with "DONE". 


## STUDENT iPod/iPhone/iPad APP 

The student application is distributed free through the Apple iTunes store.  Students should search for the application by name: "Graphing in Groups".  It runs on iPods, iPhones, and iPads. To download the app to your phone, select "Install".


## LAUNCHING THE PROGRAM 


To launch the program, double tap on the "Graphing in Groups" icon on the phone's "Home" screen.  The icon depicts two crossed lines on a graph - one green, one yellow. 


## THE WELCOME SCREEN

As the App starts, it will briefly display a credits screen, followed by a "Welcome" screen. Students should enter their name or initials in the text box then tap on the button labeled "Open Graph Window".  


## THE GRAPH SCREEN

The screen with the graph is the center of activity for Graphing in Groups.  Students can use the application individually or in groups.  The App starts out in individual mode - students can move their point around using the four arrow keys. They can change to a second point "A" and do the same. By using the "Mark" button for each point, students can create their own line between the two points.  To use Graphing in Groups with a partner, students must first connect to NetLogo on the Teacher's computer.


## CONNECTING TO NETLOGO ON THE TEACHER'S COMPUTER

From the graph window, select the "NetLogo" settings button in the upper left corner of the screen. On the settings screen, enter four additional pieces of information: 1) the teacher's "Server address", 2) the version of NetLogo the teacher is using, 3) a graph color (yellow or green) and 4) a graph number (1-8). Each student pair must choose the same graph number and the same color to be able to construct the same line.  After selecting "Save" in the upper right, the graph window will reappear with a new "Join" button on the right.  Select "Join", to connect to the teacher's computer. Your name should appear in both the "HubNet Control Center" and as a labeled point in the graph window you selected.


## MOVING AND MARKING TO PLOT LINES 

Pressing the arrow keys moves the cursor around the graph that corresponds to the group you chose.  The App displays the coordinates of your cursor as well as your partner's cursors and information about your group and color.  Tapping the "Mark" button marks the spot with an "x".  When both partners have marked a spot a line will be displayed in the color you chose.  A new line can be plotted by moving and marking a different spot on the graph.


    Plotting a Line
    ---------------
    Press arrow keys to move the cursor
    "Mark" to plot a point and a line.
    
    ---NOTE: Both partners must MARK to plot the first line.

## USING PHOTOS

STUDENTS: You can use the built-in camera to take photos that will appear as background images on the graph.  You can also select photos taken previously from their photo library.  If you are connected to the teacher's NetLogo, the photos you select will be sent to your graph window within NetLogo.

TEACHERS: Use the "Clear Images" button to erase all images from graph windows.

## CREDITS AND REFERENCES 


White, T., Wallace, M., & Lai, K. (2012). Graphing in groups: Learning about lines in a collaborative classroom network environment. Mathematical Thinking and Learning, 14(2), 149-172.

More detailed explanations of the iOS client application can be found at 
http://click.faculty.ucdavis.edu/designs/graphing-in-groups/collaborative-graphing-in-groups
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

circle 3
true
0
Circle -7500403 false true 23 23 255
Circle -7500403 false true 12 12 277
Circle -7500403 false true 2 2 297
Circle -7500403 false true 22 22 257
Circle -7500403 false true 16 15 271
Circle -7500403 false true 5 4 292
Circle -7500403 false true 5 5 291
Circle -7500403 false true 2 2 296
Circle -7500403 false true 16 17 268
Circle -7500403 false true 9 9 283
Circle -7500403 false true 6 6 289
Circle -7500403 false true 18 19 264
Circle -7500403 false true 9 10 282
Circle -7500403 false true 12 13 276
Circle -7500403 false true 9 9 282
Circle -7500403 false true 19 19 262

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

link
true
0
Line -7500403 true 150 0 150 300

link direction
true
0
Line -7500403 true 150 150 30 225
Line -7500403 true 150 150 270 225

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270

@#$#@#$#@
NetLogo 5.0.5
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
BUTTON
6
204
75
237
string
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
7
10
132
43
show-equation
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
6
55
123
88
show-partner
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
10
105
90
138
number
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
159
79
283
112
set-graph-dim
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
164
25
280
58
mark-partner
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
168
148
276
181
add-partner
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
138
190
269
223
remove-partner
NIL
NIL
1
T
OBSERVER
NIL
NIL

BUTTON
4
151
135
184
update-settings
NIL
NIL
1
T
OBSERVER
NIL
NIL

@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180

@#$#@#$#@
0
@#$#@#$#@
