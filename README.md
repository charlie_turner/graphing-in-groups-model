# graphing-in-groups-model

## Graphing In Groups Hubnet Model for NetLogo 5.0.x and iOS clients

Graphing in Groups is a collaborative learning environment developed for mathematics 
classrooms. Students working in pairs construct graphs of linear functions by using 
their respective iOS devices to move points in a shared Cartesian window. The Graphing 
in Groups hubnet model allows a teacher to view and display the graphing windows for all 
student pairs. Two pairs, one yellow and one green, can form lines on in each graphing 
window. As students move their points in their respective graphing windows, they 
can mark select locations to form lines, the *y=mx+b* equation for which is displayed 
above or below the graph. This configuration allows students to participate in a 
variety of structured or open-ended collaborative mathematics tasks involving 
generating lines with particular properties—a specific slope or intercept, a 
parallel or perpendicular relationship to another group’s line, passage through a 
select point, and so on.

It relies on two custom NetLogo extensions:  
**clickgraph-extension** (https://bitbucket.org/charlie_turner/clickgraph-extension) and  
**hubnet-web** (https://bitbucket.org/charlie_turner/hubnet-web-extension)

It also relies on a custom iOS client that can be obtained from the iTunes Store.

## CONTENTS:
This repository contains a folder GraphingInGroups that in turn contains
one NetLogo model file: GraphingInGroups.nlogo.  

## TO USE:

1. Acquire the NetLogo software (5.0.5 is the latest version as of this writing).
2. Acquire and install the clickgraph-extension and hubnet-web-extensions.
3. Clone this repository onto your machine.
4. Place the GraphingInGroups folder in the models folder of NetLogo.
5. Open the model: GraphingInGroups > GraphingInGroups.nlogo from within NetLogo
6. Follow the instructions in the models "How To Use It" section of the on-line documentation.

## NOTES:

The GraphingInGroups.nlogo model is distributed within a folder of the same name because
the model generates multiple log files with each use; outputing those within a folder 
keeps the NetLogo model folder cleaner.

This model follows the NetLogo hubnet model of distributed collaboration.  As such, it does very
little without clients.  The production clients intended for use with this model run on a 
number of different iOS mobile devices (iPhone, iPad, etc.).  Clients communicate with NetLogo
using the hubnet protocol running over websockets.  This protocol is implemented in the hubnet-web
extension.  

A development version of the client is also available.  It can be run from within a brower
that supports the correct version of websockets.  The development version of the client is
available upon request.

## QUESTIONS:
If you run into problems or have questions about the extension, please email me: cjturner@ucdavis.edu
I have tested early versions of this code on Mac only. 




